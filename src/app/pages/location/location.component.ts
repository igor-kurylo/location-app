import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Errors} from '../../models/errors';

declare var google: any;

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  public locationForm: FormGroup;

  public map;
  public service;
  public places = [];
  public request = {
    query: '',
    fields: ['photos', 'formatted_address', 'name', 'rating', 'opening_hours', 'geometry'],
  };
  public lat: number;
  public lng: number;
  public loading: boolean = false;
  public markers = [];
  public error: Errors = new Errors();

  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.locationForm = this.fb.group({
      location: ['', Validators.required]
    });

    this.getCurrentLocation();
  }

  /**
   * initialization google map and get history list
   */
  initMap() {
    let mapCenter = new google.maps.LatLng(this.lat, this.lng);
    this.places = JSON.parse(window.localStorage.getItem('app-places')) || [];

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: mapCenter,
      zoom: 15
    });

    this.loading = false;
  }

  /**
   * add new place on map and history
   */
  addPlace() {
    this.request.query = this.locationForm.value.location;
    this.service = new google.maps.places.PlacesService(this.map);
    this.service.findPlaceFromQuery(this.request, this.callback.bind(this));
  }

  /**
   * show place on map from history
   * @param id
   */
  showPlace(id) {
    const lat = this.places[id].coords.lat;
    const lng = this.places[id].coords.lng;

    this.clearAllPlaces();

    let marker = new google.maps.Marker({
      position: {lat: lat, lng: lng},
      map: this.map,
      title: this.places[id].name
    });
    this.markers.push(marker);

    this.map.setCenter({lat: lat, lng: lng});
    marker.setMap(this.map);
  }

  /**
   * clear all markers place
   */
  clearAllPlaces() {
    this.markers.forEach(item => {
      item.setMap(null);
    });
  }

  /**
   * remove place from history
   * @param {number} id
   */
  removePlace(id: number) {
    const message = 'Do you want to remove this place?';
    if (window.confirm(message)) {
      this.places.splice(id, 1);
      window.localStorage.setItem('app-places', JSON.stringify(this.places));
      this.ref.detectChanges();
    }
  }

  /**
   * get current user location
   */
  getCurrentLocation() {
    this.loading = true;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;

        this.initMap();
      });
    }

    this.lat = 47.837326;
    this.lng = 35.137689;
    this.initMap();
  }

  /**
   * callback (add marker place on map and save information place to storage)
   * @param results
   * @param status
   */
  callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
      this.error.notFound = false;

      for (let i = 0; i < results.length; i++) {
        let place = results[i],
          placeURI = encodeURI(this.request.query),
          lat = place.geometry.location.lat(),
          lng = place.geometry.location.lng();

        this.places.unshift({
          name: place.name,
          address: place.formatted_address,
          coords: {lat, lng},
          link: `https://www.google.com/maps/search/?api=1&query=${placeURI}`
        });

        let marker = new google.maps.Marker({
          position: {lat: lat, lng: lng},
          map: this.map,
          title: place.name
        });
        this.markers.push(marker);

        this.clearAllPlaces();

        this.map.setCenter({lat: lat, lng: lng});
        marker.setMap(this.map);
      }
      window.localStorage.setItem('app-places', JSON.stringify(this.places));
      this.ref.detectChanges();
    } else {
      this.error.notFound = true;
      this.ref.detectChanges();
    }
  }

}
